#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-10-29 11:26:25

#include "Arduino.h"
#include "simpleReflowController.h"
#include "simpleReflowController.h"
#include "simpleReflowController.h"

void initTFT() ;
void clearTFT() ;
void setHeader(int pCurrentTemp, int pSec, int pProfileTemp) ;
void setProfile() ;
void setGraph(String pName, int pSec) ;
void setStatus(String pText) ;
void setup() ;
void loop() ;
void initPorts() ;
void secIncrease() ;
profileStep getProfile(int pSec) ;
void initTermocouple() ;
int getTemp() ;


#include "display.ino"
#include "simpleReflowController.ino"
#include "thermocouple.ino"

#endif
