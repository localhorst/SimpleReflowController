#include "simpleReflowController.h"

#define tcUpdateInterval 100

MAX31855 tc1(MAXCS1, tcUpdateInterval);
MAX31855 tc2(MAXCS2, tcUpdateInterval);

void initTermocouple() {
	tc1.setup();
	tc2.setup();
}

int getTemp() {
	//TODO Kalman filter
	tc1.update();
	tc2.update();

	if (tc1.getStatus() != 0) {
		Serial.println("Error tc1");
	}

	if (tc2.getStatus() != 0) {
		Serial.println("Error tc2");
	}

	return (int) ((tc1.getTemperature() + tc2.getTemperature()) / 2);
}

