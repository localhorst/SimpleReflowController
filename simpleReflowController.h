/*
 * simpleReflowController.h
 *
 *  Created on: 26.10.2018
 *      Author: hendrik
 */

#ifndef SIMPLEREFLOWCONTROLLER_H_
#define SIMPLEREFLOWCONTROLLER_H_

//#include "Adafruit_MAX31855.h"
#include "MAX31855.h"
#include "Arduino.h"
#include "TimerOne.h"
//#include <SPI.h>
#include <TFT.h>

#include <FastLED.h>
//#include "Adafruit_GFX.h"
//#include "Adafruit_ST7735.h"



#define MAXSO 12
#define MAXCS1 6
#define MAXCS2 7
#define MAXCLK 13

#define PIEZO 2
#define SSR1 4
#define SSR2 5

#define TFTCS 10
#define TFTDC 9
#define TFTRST 8

#define NUM_LEDS 1

#define DATA_PIN 3
#define CLOCK_PIN 13

unsigned int historyTemp[320];

struct profileStep {
  int temp;
  String name;
  int starttime;
  int endtime;
};


#endif /* SIMPLEREFLOWCONTROLLER_H_ */
