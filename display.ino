#include "simpleReflowController.h"

TFT myScreen = TFT(TFTCS, TFTDC, TFTRST);



void initTFT() {
	myScreen.begin();
	myScreen.background(0, 0, 0);
	//myScreen.setTextSize(2);
}

void clearTFT() {
	//myScreen.background(0, 0, 0);
	myScreen.fillRect(0, 0, 160, 128, 0);

}

void setHeader(int pCurrentTemp, int pSec, int pProfileTemp) {

	String sCurrentTemp = String(pCurrentTemp);
	char cCurrentTemp[4];
	sCurrentTemp.toCharArray(cCurrentTemp, 4);
	//Serial.println(cCurrentTemp);

	String sSec = String(pSec);
	char cSec[4];
	sSec.toCharArray(cSec, 4);
	//Serial.println(cSec);

	String sProfileTemp = String(pProfileTemp);
	char cProfileTemp[4];
	sProfileTemp.toCharArray(cProfileTemp, 4);
	//Serial.println(cProfileTemp);



	myScreen.stroke(255, 255, 255);
	myScreen.setTextSize(2);

	myScreen.text(cCurrentTemp, 0, 0);
	myScreen.text(cSec, 55, 0);
	myScreen.text(cProfileTemp, 110, 0);
	myScreen.stroke(0, 0, 255); // set the stroke color to red
	myScreen.line(0, 17, 160, 17); // draw a line across the screen
	myScreen.line(0, 18, 160, 18); // draw a line across the screen

}
void setProfile() {
	myScreen.stroke(255, 255, 255);
	for (int i = 0; i <= 160; i++) {
		Serial.println(i);
		int x = i;
		double temp = getProfile(i * 2).temp;
		int y = map(temp, 0, 320, 109, 19);
		myScreen.point(x, y);
	}
}

void setGraph(String pName, int pSec) {
	char cName[20];
	pName.toCharArray(cName, 20);

	myScreen.stroke(255, 255, 255);
	myScreen.setTextSize(1);
	myScreen.text(cName, 0, 25);

	myScreen.stroke(255, 0, 0);
	for (int i = 0; i <= pSec; i++) {
		Serial.println(i);
		int x = i;
		double temp = historyTemp[i *2];
		int y = map(temp, 0, 320, 109, 19);
		myScreen.point(x, y);
	}




}

void setStatus(String pText) {
	char cStatus[20];
	pText.toCharArray(cStatus, 20);
	myScreen.stroke(255, 255, 255);
	myScreen.setTextSize(2);
	myScreen.text(cStatus, 0, 114);
	myScreen.stroke(0, 0, 255); // set the stroke color to red
	myScreen.line(0, 110, myScreen.width(), 110); // draw a line across the screen
	myScreen.line(0, 111, myScreen.width(), 111); // draw a line across the screen
}
